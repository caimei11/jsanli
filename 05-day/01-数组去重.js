// var arr =[1,2,3,3,2,'hello','hello'];
// // 1.双层for循环
// function unique(arr){
//     // 第一次控制第一个数
//     for(let i=0;i<arr.length;i++){
//         // 第二层控制第二个数
//         for(let j=i+1;j<arr.length;j++){
//             if(arr[i]===arr[j]){
//                 arr.splice(j,1);
//                 // 修改下标
//                 j--;
//             }
//         }
//     }
// }

// unique(arr);
// console.log(arr); //[ 1, 2, 3, 'hello' ] splice修改原数组

// 2.indexOf() 3.LastIndexOf()
var arr =[1,2,3,3,2,'hello','hello'];
function unique(arr){
    let newArr=[];
    for(let i=0;i<arr.length;i++){
        if(newArr.indexOf(arr[i])===-1){
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

console.log(unique(arr));

// 4. includes
// 参数：想要查找的数组元素 返回值:false/true
// var arr =[1,2,3,3,2,'hello','hello'];
// function unique(arr){
//     let newArr=[];
//     for(let i=0;i<arr.length;i++){
//         if(!newArr.includes(arr[i])){
//             newArr.push(arr[i]);
//         }
//     }
//     return newArr;
// }

// console.log(unique(arr));

// 5.forEach＋indexOf
var arr =[1,2,3,3,2,'hello','hello'];
function unique(arr){
    let newArr=[];
    arr.forEach(function(item){
        if(newArr.indexOf(item)===-1){
            newArr.push(item);
        }
    })
    return newArr;
}
console.log(unique(arr));

// 6.用filter+indexOf/includes
var arr = [1, 2, 3, 3, 2, 'hello', 'hello'];
function unique(arr) {
    let newArr = [];
    arr.filter(function (item) {
        return newArr.includes(item) ? "" : newArr.push(item);
    })
   
    return newArr;
}
console.log(unique(arr));

// 7.set去重  成员是唯一
var arr=[1,2,3,3,2,'hello','hello'];
let set=new Set(arr);
console.log(set); //Set(4) { 1, 2, 3, 'hello' }
console.log(Array.from(set));  //[ 1, 2, 3, 'hello' ]
console.log([...set]);//[ 1, 2, 3, 'hello' ]

// map的使用
var arr=[
    {
        id:1,
        name:"zhangsan"
    },{
        id:2,
        name:"lisi"
    },{
        id:3,
        name:"wangwu"
    }
];
var res=arr.map(function(item){
    return item.name
});
console.log(res); //[ 'zhangsan', 'lisi', 'wangwu' ]