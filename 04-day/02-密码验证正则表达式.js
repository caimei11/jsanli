/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-16 11:24:09
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-23 09:48:33
 * @FilePath: \vscode练习\作业\04-day\02-密码验证正则表达式.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 密码验证
// 匹配密码，必须包含大写，小写和数字, 和特殊字符(!,@,#,%,&), 且大于6位
// 1.(?=.*[A-Z] )(?=.*[a-z])(?=.*\d)(?=.*[!@#%&])
// 2.^[a-zA-Z\d!@#%&]{6,}$
// .*任意字符
var reg=/(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[!@#%&])^[a-zA-Z\d!@#%&]{6,}$/;
console.log(reg.test('Aa@1232'));