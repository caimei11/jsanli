/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-16 11:06:36
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-23 09:47:55
 * @FilePath: \vscode练习\作业\04-day\01-手机号正则表达.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 1.验证是否为11位有效手机号码？
//以1为开头
//第二位为3，4，5，7，8中的任意一位
// 最后以0 - 9的9个整数结尾
var phone = /^[1][34578]\d{9}$/;
console.log(phone.test('1334567890'));

