 // 作业3 封装函数实现10的阶乘 10=10*9*8*7*6*5*4*3*2*1=3628800;
 function Sum(num) {
    var sum = 1;

    // str = '10='
    // 模板字符串 `${}`
    var str = `${num}=`;
    for (var i = num; i > 0; i--) {
        sum = sum * i;
        if (i == 1) {
            str += i + '=';
        } else {
            str += i + '*';
        }
    }
    return str + sum;
}

console.log(Sum(10));