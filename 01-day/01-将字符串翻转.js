// 作业1 var str='i love china';将字符串反转'china love i';
//  反转字符串 
//  方法一：
var str = 'i&love&china';
// 转为数组split切割字符串转为数组
var result = str.split('&');
// for (var i = result.length - 1; i >= 0; i--) {
//     if (i == result.length - 1) {
//         console.log(result[i] + " " + result[i - 1] + " " + result[i - 2]);
//     }
// }
// 方法二
var result = str.split('&');
var res = result.reverse().join(' ');
console.log(res);