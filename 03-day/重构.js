/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-16 09:05:43
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-23 09:45:10
 * @FilePath: \vscode练习\作业\03-day\重构.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// // 1.重构pop
// // 参数：无 返回值：删除的数组元素，是否修改原数组：修改
var arr = [1, 2, 3, 4];
Array.prototype.myPop = function () {
    // 考虑数组为空
    if (this.length > 0) {
        let last = this[this.length - 1];
        this.length--;
        return last;
    } else {
        return undefined;
    }
}
var res = arr.myPop();
console.log(res, arr);//4 [ 1, 2, 3 ]


// 2.重构push方法
// 参数：任何想要添加的数据类型 返回值：新数组的长度，修改原数组
var arr=[1,2,3,4];
Array.prototype.myPush=function(){
    for(let i=0;i<arguments.length;i++){
        this[this.length]=arguments[i];
    }
    return this.length;
}
var res=arr.myPush('hello',12,57);
console.log(res,arr); //7 [ 1, 2, 3, 4, 'hello', 12, 57 ]

// // 3.重构shift方法
// // 参数：无 返回值：删除掉的数组元素 修改原数组
var arr = [1, 2, 3, 4];
Array.prototype.myShift = function () {
    let first = this[0];
    for (let i = 0; i < this.length; i++) {
        this[i] = this[i + 1];

    }
    this.length--;
    return first;
}

var res = arr.myShift();
console.log(res, arr); //1 [ 2, 3, 4 ]

// // 4.重构unshift方法
// // 参数：任何想要添加的数组元素  返回值：新的数组长度，修改原数组
var arr = [1, 2, 3, 4];
Array.prototype.myUnshift = function () {
    // 计算新数组长度
    let sum = this.length + arguments.length;
    for (let i = sum; i > 0; i--) {
        // 将原数组的元素向后移动 auguments.length
        if (i > arguments.length) {
            this[i - 1] = this[i - 1 - arguments.length];

        } else {
            this[i - 1] = arguments[i - 1];
        }
    }
    return sum;
}

var res = arr.myUnshift('hello', 45);
console.log(res, arr); //6 [ 'hello', 45, 1, 2, 3, 4 ]

// 五个迭代方法
// // 1.every
// // 参数：函数(item,index,arr)  返回值：true 或者false;

// var arr = [1, 2, 3, 4];
// Array.prototype.myEvery = function (fun, obj) {
//     for (let i = 0; i < this.length; i++) {
//         if (!(obj ? fun.call(obj, this[i], i, this) : fun(this[i], i, this))) {
//             return false;
//         }
//     }
//     return true;
// }

// var res = arr.myEvery(function (item, index, arr) {
//     // 只要有一个不满足条件，跳出循环 不再向下判断
//     console.log(this);// 四次[String: 'hello']
//     // 只要写了第二个参数 this-->第二个参数
//     return item > 0;
// }, ('hello'))
// console.log(res);  //true

// // 2.some
// // 只要有一个满足一个条件  返回true 跳出循环
var arr = [1, 2, 3, 4];
Array.prototype.mySome = function (fun, obj) {
    for (let i = 0; i < this.length; i++) {
        if (obj ? fun.call(obj, this[i], i, this) : fun(this[i], i, this)) {
            return true;
        }

    }
    return false;
}

var res = arr.mySome(function (item, index, arr) {
    console.log(this);  //两次zhangsan
    return item > 1;
}, { name: "zhangsan" });
console.log(res); //true



// 3.map 映射对每一个数组元素操作
// 参数：函数(item,index,arr) 返回值：操作后的新数组
var arr = [1, 2, 3, 4];
Array.prototype.myMap = function (fun, obj) {
    let newArr = [];
    for (let i = 0; i < this.length; i++) {
        newArr.push(obj ? fun.call(obj, this[i], i, this) : fun(this[i], i, this))
    }
    return newArr;
}
var res = arr.myMap(function (item, index, arr) {
    console.log(this); //四次zhangsan
    return item + 1;
}, { name: "zhangsan" });
console.log(res); //[ 2, 3, 4, 5 ]

// // 4.filter 过滤符合条件的数组元素组成新的数组
// // 返回值：新数组 参数(item,index,arr),this指向变化
// var arr = [1, 2, 3, 4];
// Array.prototype.myFilter = function (fun, obj) {
//     let newArr = [];
//     for (let i = 0; i < this.length; i++) {
//         if (obj ? fun.call(obj, this[i],i,this) : fun(this[i],i,this)) {
//             newArr.push(this[i]);
//         }
//     }
//     return newArr;
// }
// var res = arr.myFilter(function (item, index, arr) {
//     console.log(this);  //四次zhangsan
//     return item > 2;
// }, { name: "zhangsan" });
// console.log(res); //[3,4]


// 5.forEach 遍历数组
var arr = [1, 2, 3, 4];
Array.prototype.myForEach = function (fun, obj) {
    for (let i = 0; i < this.length; i++) {
        obj ? fun.call(obj, this[i]) : fun(this[i])
    }
}
var res = arr.myForEach(function (item, index, arr) {
    // console.log(this); //四次zhangsan
    console.log(index + ' -- ' + item);
    // undefined -- 1
    // undefined -- 2
    // undefined -- 3
    // undefined -- 4
}, { name: "zhangsan" });
