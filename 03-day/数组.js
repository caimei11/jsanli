/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-14 17:05:48
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-17 11:12:44
 * @FilePath: \vscode练习\作业\03-day\数组.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// push 向数组里面添加一个或多个函数，返回这个新的数组
// var numbers = [2, 8, 2, 5, 8];
// Array.prototype.myPush = function () {
//     for (var i = 0; i < arguments.length; i++) {
//         this[this.length] = arguments[i];
//     }
//     return this;
// }
// console.log(numbers.myPush('a', 34));



// // pop 删除数组的最后一个元素，并返回这个元素
// var numbers = [2, 8, 56, 5, 11];
// Array.prototype.myPop = function () {
//     arr = [];
//     var num = this[this.length - 1];
//     for (let i = 0; i < this.length - 1; i++) {
//         arr[arr.length] = this[i];
//     }
//     this.length = arr.length;
//     return num;

// }
// console.log(numbers.myPop()); //11
// console.log(numbers); //[ 2, 8, 56, 5 ]

// shift 删除第一个元素，并返回这个元素
// var numbers = [2, 8, 56, 5, 11];
// Array.prototype.myShift = function () {
//     arr = [];
//     var num = this[0];
//     for (var i = 1; i < this.length; i++) {
//         arr[arr.length] = this[i];
//         this[i - 1] = arr[i - 1];

//     }
//     this.length = arr.length;
//     return num;

// }
// console.log(numbers.myShift());
// console.log(numbers.myShift());
// console.log(numbers.length);

// unshift 向数组的开头添加一个或多个元素，并返回新的长度
var numbers = [2, 8, 56, 5, 11];
Array.prototype.myUnshift = function () {
    temp = [];
    for (var i = 0; i < arguments.length; i++) {
        temp[temp.length] = arguments[i];
    }

    for (var j = 0; j < this.length; j++) {
        temp[temp.length] = this[j];

    }
    this.length = temp.length;
    for (var k = 0; k < temp.length; k++) {
        this[k] = temp[k];

    }
    return this.length;
}
console.log(numbers.myUnshift(1, 10, 111)); //8
console.log(numbers);
// [
//     1, 10, 111,  2,
//     8, 56,   5, 11
//   ]





// every() 方法用于检测数组所有元素是否都符合指定条件（通过函数提供）。
// var numbers = [2, 8, 56, 5, 11];
// Array.prototype.myEvery = function (fn, thisValue) {
//     // if (typeof fn != 'function') {
//     //     return 'This is not function';
//     // }
//     if (typeof fn() == 'boolean') {
//         for (var i = 0; i < this.length; i++) {
//             var result = fn.call(thisValue, this[i]);

//         }


//     }

// }



// var res = numbers.myEvery(function (a) {
//     return a > 2;

// })
// console.log(res);


// //fruits输出结果：Banana,Orange,Lemon,Kiwi,Apple,Mango

// // function test(a, b) {
// //     a = 1;
// //     b(a);
// //     console.log(b(a));
// // }

// // test(1, function (c) {
// //     console.log(c);
// // })
