/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-20 21:47:43
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-23 10:41:20
 * @FilePath: \vscode练习\作业\06-day\02-实现数组扁平输出.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 2.编写代码实现数组扁平化输出，例如：输入[[1,2,3,[4,5,6,[7,8]]],[9,10],[11,12,[13]],5] 输出为[1,2,3,4,5,6,7,8,9,10,11,12,13,5]

// 第一种方法
function floatDeep(arr) {
    // join也可以转成字符串
    var result = arr.toString().split(',').map(function (item) {
        return Number(item);
    });

    return result;
}

var array = [[1, 2, 3, [4, 5, 6, [7, 8]]], [9, 10], [11, 12, [13]], 5];
console.log(floatDeep(array));

// 第二种方法
// 递归
function floatDeep(arr) {
    var res = [];
    for (var i = 0; i < arr.length; i++) {
        if (Array.isArray(arr[i])) {
            res = res.concat(floatDeep(arr[i]));
        } else {
            res.push(arr[i]);
        }

    }

    return res;
}

var arr = [[1, 2, 3, [4, 5, 6, [7, 8]]], [9, 10], [11, 12, [13]], 5];
console.log(floatDeep(arr));
// console.log(Array.isArray(arr));
