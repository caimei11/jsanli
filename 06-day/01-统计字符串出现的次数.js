/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-20 21:47:24
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-23 10:35:51
 * @FilePath: \vscode练习\作业\06-day\01-统计字符串出现的次数.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 1.统计一个字符串中字符出现的次数，例如：输入 hello,l 输出 2
// function Strnumber(str, num) {
//     var sum = 0;
//     for (var i = 0; i < str.length; i++) {
//         if (str[i] ===num) {
//             sum = sum + 1;
//         }
//     }
//     return sum;
// }

// console.log(Strnumber('hello', 'l')); 

var str = 'hello';
function Strmount(str, a) {
    var reg = new RegExp(a, 'g');
    str = str.match(reg);
    return str.length;
}


console.log(Strmount(str, 'l'));

