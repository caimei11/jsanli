/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-20 21:49:48
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-23 10:41:51
 * @FilePath: \vscode练习\作业\06-day\03-返回字符串最好一单词长度.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 3.给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。
// 单词 是指仅由字母组成、不包含任何空格字符的最大子字符串。
// 示例 1：输入：s = "Hello World"
// 输出：5
// 解释：最后一个单词是“World”，长度为5。
// 示例 2：
// 输入：s = "   fly me   to   the moon  "
// 输出：4
// 解释：最后一个单词是“moon”，长度为4。
// 示例 3：
// 输入：s = "luffy is still joyboy"
// 输出：6
// 解释：最后一个单词是长度为6的“joyboy”。
// 提示：
// 1 <= s.length <= 104
// s 仅有英文字母和空格 ' ' 组成
// s 中至少存在一个单词
function LastLength(arr) {
    var str = arr.split(' ');
    var newArr = [];
    for (var i = 0; i < str.length; i++) {
        if (!str[i] == ' ') {
            newArr.push(str[i]);
        }
    }
    var last = newArr[newArr.length - 1];
    console.log(last.length);

}

var s = 'a';
LastLength(s);


var str = 'a';
function StrLength(str) {
    // trim 去除空格
    str = str.trim().split(' ');
    var last = 0;
    last = str[str.length - 1].length;
    return last;
}

console.log(StrLength(str));






