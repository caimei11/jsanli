/*
 * @Author: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @Date: 2022-08-10 16:27:51
 * @LastEditors: lihaha 11238063+caimei11@user.noreply.gitee.com
 * @LastEditTime: 2022-08-11 09:16:14
 * @FilePath: \vscode练习\作业\02-day\01-数组和等于target并输出下标.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

// 1.给定一个数组arr=[2,5,7,11],target=9,要求实现数组中的任意元素相加之和等于target并输出下标
var arr = [2, 5, 7, 11];
var target = 9;
function twoSum(arr, target) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length; j++) {
            if (arr[i] + arr[j] == target) {
                return [i, j]
            }
        }
    }

    return [-1, 1];
}


console.log(twoSum(arr, target));